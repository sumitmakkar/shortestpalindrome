#include <iostream>

using namespace std;

class Engine
{
    private:
        string str;
    
        string reverseString(string str)
        {
            int back  = (int)str.length() - 1;
            int front = 0;
            while(front < back)
            {
                char temp  = str[front];
                str[front] = str[back];
                str[back]  = temp;
                front++;
                back--;
            }
            return str;
        }
    
        string prependString(string sourceStr , string toBePrependedStr , int tillIndex)
        {
            string resultStr;
            int    len = (int)sourceStr.length();
            for(int i = 0 ; i < tillIndex ; i++)
            {
                resultStr += toBePrependedStr[i];
            }
            for(int i = 0 ; i < len ; i++)
            {
                resultStr += sourceStr[i];
            }
            return resultStr;
        }
    
    public:
        Engine(string s)
        {
            str = s;
        }
    
        string makeStringPalindrome()
        {
            string reversedString = reverseString(str);
            int    len            = (int)reversedString.length();
            int    requiredStringIndex = (int)reversedString.length();
            for(int i = 0 ; i < len ; i++)
            {
                int  originalStrIndex = 0;
                int  reversedStrIndex = i;
                bool breakFlag       = false;
                while(str[originalStrIndex] == reversedString[reversedStrIndex])
                {
                    originalStrIndex++;
                    reversedStrIndex++;
                    if(reversedStrIndex == len)
                    {
                        requiredStringIndex = i;
                        breakFlag           = true;
                        break;
                    }
                }
                if(breakFlag)
                {
                    break;
                }
            }
            return (prependString(str , reversedString , requiredStringIndex));
        }
};

int main(int argc, const char * argv[])
{
    string str = "abcd";
    Engine e = Engine(str);
    cout<<e.makeStringPalindrome()<<endl;
    return 0;
}
